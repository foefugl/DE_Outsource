using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class HHTimeScalerTLBehaviour : PlayableBehaviour
{
    public float ScaleValue = 1;
}
