%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Ann_Face
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Ann_Drone_model
    m_Weight: 1
  - m_Path: Rig_root
    m_Weight: 0
  - m_Path: Rig_root/Head
    m_Weight: 0
  - m_Path: Rig_root/Head/Back_root
    m_Weight: 0
  - m_Path: Rig_root/Head/Back_root/Back
    m_Weight: 0
  - m_Path: Rig_root/Head/Back_root/Back/Point_FX
    m_Weight: 0
  - m_Path: Rig_root/Head/HairB_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairB_root/HairB_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairB_root/HairB_01/HairB_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairB_root/HairB_01/HairB_02/HairB_03
    m_Weight: 0
  - m_Path: Rig_root/Head/HairF_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairF_root/HairF_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairF_root/HairF_01/HairF_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFL_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFL_root/HairFL_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFL_root/HairFL_01/HairFL_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFL_root/HairFL_01/HairFL_02/HairFL_03
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFR_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFR_root/HairFR_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFR_root/HairFR_01/HairFR_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairFR_root/HairFR_01/HairFR_02/HairFR_03
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL1_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL1_root/HairL1_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL1_root/HairL1_01/HairL1_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL1_root/HairL1_01/HairL1_02/HairL1_03
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL1_root/HairL1_01/HairL1_02/HairL1_03/HairL1_04
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL1_root/HairL1_01/HairL1_02/HairL1_03/HairL1_04/HairL1_05
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL2_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL2_root/HairL2_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairL2_root/HairL2_01/HairL2_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR1_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR1_root/HairR1_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR1_root/HairR1_01/HairR1_02
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR1_root/HairR1_01/HairR1_02/HairR1_03
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR1_root/HairR1_01/HairR1_02/HairR1_03/HairR1_04
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR1_root/HairR1_01/HairR1_02/HairR1_03/HairR1_04/HairR1_05
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR2_root
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR2_root/HairR2_01
    m_Weight: 0
  - m_Path: Rig_root/Head/HairR2_root/HairR2_01/HairR2_02
    m_Weight: 0
  - m_Path: Rig_root/Head/Hat_root
    m_Weight: 0
  - m_Path: Rig_root/Head/Hat_root/Hat_01
    m_Weight: 0
  - m_Path: Rig_root/Head/Hat_root/Hat_01/Hat_02
    m_Weight: 0
