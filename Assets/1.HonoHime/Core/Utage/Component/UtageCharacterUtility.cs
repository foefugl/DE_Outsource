namespace UtageExtensions {
    public class UtageCharacterUtility {
        public enum Perform 
        {
            Proud,
            PointForward,
            Explain,
            Thinking,
            Hello,
            Jump,
            RaiseHand
        }
        public enum Face
        {
            Sad,
            Angry,
            Surprise,
            Happy,
            FaceDefault,
            Smile
        }
        public enum Emoji
        {
            Notice,
            Angry
        }
        public enum PP
        {
            SpeedLine
        }
        public enum AdvCommandType 
        {
            Perform,
            Face,
            Emoji,
            PPEffect,
            Anim,
            Move,
            None
        }
    }
}